# GoogleHashCode 2022 Team Code

* This [Code](Team Code.py) is my team's attempt at the Google Hash Code 2022 competition.
* Although the code is far from perfect, it represents our engagement and skills so I decided sharing it is worthwile.
* For more information on the competition please visit the website https://codingcompetitions.withgoogle.com/hashcode.
