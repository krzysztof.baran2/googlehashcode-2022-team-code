import sys

class Contributor(object):
    def __init__(self, name, abilities):
        self.name = name
        self.abilities = abilities
        self.type = {}
    
    def increment(self, skill):
        self.type[skill] += 1
    
    def suitable(self, skill, Project):
        try:
            return (self.type[skill] + 1 == Project.needs[skill] or self.type[skill] >= Project.needs[skill])
        except KeyError:
            return False

class Project(object):
    def __init__(self, name, days, score, best, roles):
        self.name = name
        self.days = days
        self.score = score
        self.best = best
        self.roles = roles
        self.needs = {}

with open("a_an_example.in.txt") as f:
    lines = f.read().splitlines()

line_1 = lines[0].split()
num_contributors = int(line_1[0])
num_projects = int(line_1[1])

i = 0
list_contributors = []
track = 1
while i < num_contributors:
    personInfo = lines[track].split()
    list_contributors.append(Contributor(personInfo[0], int(personInfo[1])))
    track += 1
    j = 0
    while j < list_contributors[i].abilities:
        l = lines[track].split()
        skill = l[0]
        level = l[1]
        list_contributors[i].type[skill] = int(level)
        j += 1
        track += 1
    i += 1

i = 0
list_projects = []
while i < num_projects:
    projectInfo = lines[track].split()
    list_projects.append(Project(projectInfo[0], int(projectInfo[1]), int(projectInfo[2]), int(projectInfo[3]), int(projectInfo[4])))
    track += 1
    j = 0
    while j < list_projects[i].roles:
        l = lines[track].split()
        skill = l[0]
        level = l[1]
        list_projects[i].needs[skill] = int(level)
        j += 1
        track += 1
    i += 1
#print("*****Contributors")
#for j in list_contributors:
#    print(j.name, j.abilities, j.type)
#print("*****Projects")
#for i in list_projects:
#    print(i.name, i.days, i.score, i.best, i.roles, i.needs)

d = {}
d2 = []
#We need to output a number of projects completed, following by pairs of "project name" and "project contributor list"
for i in list_projects:
    l = []
    d[i.name] = l
    for j in i.needs.keys():
        for k in list_contributors:
            if k.suitable(j, i) is True:
                d[i.name].append(k.name)
    d2.append(i.name)

with open("solution_example.txt", "w") as g:
    g.write(str(len(d2)) + "\n")
    l2 = []
    for k in d:
        g.write(k + "\n")
        g.write(" ".join(d[k]) + "\n")